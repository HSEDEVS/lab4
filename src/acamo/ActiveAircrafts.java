package acamo;

import messer.BasicAircraft;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;

//TODO: create hash map and complete all operations
public class ActiveAircrafts implements Observer
{
	private HashMap<String, BasicAircraft> activeAircrafts;				// store the basic aircraft and use its Icao as key
																		// replace K and V with the correct class names

	public ActiveAircrafts () {
		HashMap<String, BasicAircraft> activeAircrafts = new HashMap<String, BasicAircraft>();
		this.activeAircrafts = activeAircrafts;
	}

	public synchronized void store(String icao, BasicAircraft ac) {
		activeAircrafts.put(icao, ac);
		notify();
	}

	public synchronized void clear() {
		activeAircrafts.clear();
	}

	public synchronized BasicAircraft retrieve(String icao) {
		if(activeAircrafts.isEmpty()) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		return (activeAircrafts.get(icao));
	}

	public synchronized ArrayList<BasicAircraft> values () {
		ArrayList<BasicAircraft> al;
		al = (ArrayList<BasicAircraft>)activeAircrafts.values();
		return al;
	}

	public String toString () {
		return activeAircrafts.toString();
	}

	@Override
	// TODO: store arg in hashmap using the method above
	public void update(Observable o, Object arg) {
		BasicAircraft tmp = (BasicAircraft)arg;
		store(tmp.getIcao(), tmp);
		clear();
		
	}
}