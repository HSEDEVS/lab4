package acamo;


import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import jsonstream.PlaneDataServer;
import messer.BasicAircraft;
import messer.Messer;
import senser.Senser;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

public class Acamo extends Application implements Observer
{
	private ActiveAircrafts activeAircrafts;
    private TableView<BasicAircraft> table = new TableView<BasicAircraft>();
    private ObservableList<BasicAircraft> aircraftList = FXCollections.observableArrayList();
    private ArrayList<String> fields;
 
    private double latitude = 48.7433425;
    private double longitude = 9.3201122;
    private static boolean haveConnection = true ;
    
    public static void main(String[] args) {
		launch(args);
    }
 
    @Override
    public void start(Stage stage) {
		String urlString = "https://public-api.adsbexchange.com/VirtualRadar/AircraftList.json";
		PlaneDataServer server;
		
		if(haveConnection)
			server = new PlaneDataServer(urlString, latitude, longitude, 50);
		else
			server = new PlaneDataServer();
		
		new Thread(server).start();

		Senser senser = new Senser(server);
		new Thread(senser).start();
		
		Messer messer = new Messer();
		senser.addObserver(messer);
		new Thread(messer).start();

		// TODO: create activeAircrafts
		activeAircrafts = new ActiveAircrafts();
		
		// TODO: activeAircrafts and Acamo needs to observe messer

        messer.addObserver(activeAircrafts);
        messer.addObserver(this);
		
        fields = BasicAircraft.getAttributesNames();

        fields = BasicAircraft.getAttributesNames();
		for (int i = 0; i < fields.size(); i++) {
			// get columns names
			TableColumn<BasicAircraft, String> column;
			column = new TableColumn<BasicAircraft, String>(fields.get(i));
			switch (column.getText()) {
			case "icao":
				column.setMinWidth(20);
				break;
			case "operator":
				column.setMinWidth(120);
				break;
			case "species":
				column.setMaxWidth(80);
				break;
			case "posTime":
				column.setMinWidth(280);
				break;
			case "coordinate":
				column.setMinWidth(280);
				break;
			case "speed":
				column.setMaxWidth(60);
				break;
			case "trak":
				column.setMaxWidth(55);
				break;
			case "altitude":
				column.setMaxWidth(75);
				break;
			default:
				column.setMinWidth(400);
				break;
			}
			//aircraftList.clear();
			// to fill the values of columns
			column.setCellValueFactory(new PropertyValueFactory<BasicAircraft, String>(fields.get(i)));
			
			table.getColumns().add(column);
			
		}
		table.setItems(aircraftList);

		table.setEditable(false);
        table.autosize();

        
        
        
        // TODO: Create layout of table and pane for selected aircraft
        final Label label = new Label("Active Aircrafts");

        StackPane root = new StackPane();
        root.setPadding(new Insets(5));
        root.getChildren().add(table);
        /*label.setFont(new Font("Arial", 20));
        final VBox vbox = new VBox();

        vbox.setSpacing(5);

        //JFrame frame = new JFrame("Active Aircrafts");
        //JPanel panel = new JPanel();
        */



        // TODO: Add event handler for selected aircraft
 
        
		Scene scene = new Scene(root, 900,400);
        stage.setScene(scene);
        stage.setTitle("Acamo");
        stage.sizeToScene();
        stage.setOnCloseRequest(e -> System.exit(0));
        stage.show();
    }

    
    // TODO: When messer updates Acamo (and activeAircrafts) the aircraftList must be updated as well
    @Override
    public void update(Observable o, Object arg) {

        //ArrayList<String> tmp = (ArrayList<String>)arg;

    }
}
