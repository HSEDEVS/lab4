package senser;

import java.util.ArrayList;

public class AircraftSentenceFactory {
    public ArrayList<AircraftSentence> fromAircraftJson(String jsonAircraftString) {
        //TODO: Get distinct aircrafts from the jsonAircraftString
        // and store them in an ArrayList


        ArrayList<AircraftSentence> dummy = new ArrayList();

        jsonAircraftString= jsonAircraftString.substring(1);
        String[] output = jsonAircraftString.split("},\\{");

        /*int numOfPlane = output.length;
        for (int i = 0; i < numOfPlane; i++) {
            dummy.add(new AircraftSentence(output[i]));
        }*/

		for (String s: output) {
            System.out.println(s);
			AircraftSentence aircraft = new AircraftSentence(s);
			dummy.add(aircraft);
		}

        return dummy;
    }
}
