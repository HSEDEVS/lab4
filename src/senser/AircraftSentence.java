package senser;

public class AircraftSentence
{
    //TODO: Create attributes

    String aircraftJson;

    //TODO: Create constructor

    public AircraftSentence(String aircraftJson){
        this.aircraftJson = aircraftJson;
    };

    //TODO: Create relevant getter methods


    public String getAircraftJson() {
        return aircraftJson;
    }

    //TODO: Overwrite toString() method to print sentence

    @Override
    public String toString() {
        return this.aircraftJson;
    }
}
