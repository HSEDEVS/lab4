package messer;

import org.json.JSONObject;
import senser.AircraftSentence;

import java.util.Date;

/* Get the following datafields out of the JSON sentence using Regex and String methods
 * and return a BasicAircraft
 * 
 * For Lab3 replace use JSO parsing instead
 * 
 * "Icao":"3C5467", matches; first part is Icao, second part is 3C5467
 * "Op":"Lufthansa matches; first part is Op, second part is Lufthansa
 * "Species":1, matches; first part is Species, second part is 1
 * "PosTime":1504179914003, matches; first part is PosTime, second part is 1504179914003
 * "Lat":49.1912, matches; first part is Lat, second part is 49.1912
 * "Long":9.3915, matches; first part is Long, second part is 9.3915
 * "Spd":420.7, matches; first part is Spd, second part is 420.7
 * "Trak":6.72, matches; first part is Trak, second part is 6.72
 * "GAlt":34135, matches; first part is GAlt, second part is 34135
 */

public class AircraftFactory {

	public BasicAircraft fromAircraftSentence (AircraftSentence sentence ) {


		String icao = null;
		String operator = null;
		int species = 0;
		Date posTime = null;
		double longitude = 0;
		double latitude = 0;
		double speed = 0;
		double trak = 0;
		int altitude = 0;


		try {


            JSONObject jsonobj = new JSONObject("{" + sentence.getAircraftJson()+ "}");

            icao = jsonobj.getString("Icao");
            operator = jsonobj.getString("Op");
            species= jsonobj.getInt("Species");
            Date posbuff = new Date(jsonobj.getLong("PosTime"));
            posTime = posbuff;
            latitude = jsonobj.getDouble("Lat");
            longitude = jsonobj.getDouble("Long");
            speed=jsonobj.getDouble("Spd");
            trak = jsonobj.getDouble("Trak");
            altitude = jsonobj.getInt("GAlt");


        }

        catch(Exception e){

        }



		// TODO: Your code goes here

        /*
		Pattern icaoexp = Pattern.compile("(?<=\\\"Icao\\\":\\\")(.*?)(?=\\\")");
		matcher = icaoexp.matcher(sentence.getAircraftJson());

		if(matcher.find()) icao = matcher.group();


        Pattern operexp = Pattern.compile("(?<=\\\"Op\\\":\\\")(.*?)(?=\\\")");
        matcher = operexp.matcher(sentence.getAircraftJson());

        if(matcher.find()) operator = matcher.group();

        Pattern speciesexp = Pattern.compile("(?<=\"Species\":)(.*?)(?=,)");
        matcher = speciesexp.matcher(sentence.getAircraftJson());

        if(matcher.find())
            species = Integer.parseInt(matcher.group());

		Pattern posexp = Pattern.compile("(?<=\"PosTime\":)(.*?)(?=,)");
		matcher = posexp.matcher(sentence.getAircraftJson());

		if(matcher.find()) {
			Date posbuff = new Date(Long.parseLong(matcher.group()));
			posTime = posbuff;
		}


		Pattern lonexp = Pattern.compile("(?<=\"Long\":)(.*?)(?=,)");
		matcher = lonexp.matcher(sentence.getAircraftJson());

		if(matcher.find())
			longitude = Double.parseDouble(matcher.group());

		Pattern latexp = Pattern.compile("(?<=\"Lat\":)(.*?)(?=,)");
		matcher = latexp.matcher(sentence.getAircraftJson());

		if(matcher.find())
			latitude = Double.parseDouble(matcher.group());

		Pattern speedexp = Pattern.compile("(?<=\"Spd\":)(.*?)(?=,)");
		matcher = speedexp.matcher(sentence.getAircraftJson());

		if(matcher.find())
			speed = Double.parseDouble(matcher.group());

		Pattern traexp = Pattern.compile("(?<=\"Trak\":)(.*?)(?=,)");
		matcher = traexp.matcher(sentence.getAircraftJson());

		if(matcher.find())
			trak = Double.parseDouble(matcher.group());

		Pattern altexp = Pattern.compile("(?<=\"GAlt\":)(.*?)(?=,)");
		matcher = altexp.matcher(sentence.getAircraftJson());

		if(matcher.find())
			altitude = Integer.parseInt(matcher.group());
        */

		BasicAircraft msg = new BasicAircraft(icao, operator, species, posTime, new Coordinate(latitude, longitude), speed, trak, altitude);


		return msg;
	}
}
