package messer;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;

public class BasicAircraft {


	private String icao;
	private String operator;
	private Integer species;
	private Date posTime;
	private Coordinate coordinate;
	private Double speed;
	private Double trak;
	private Integer altitude;
	
	//TODO: Create constructor

	public  BasicAircraft(){}

	public BasicAircraft(String icao, String operator, Integer species, Date posTime, Coordinate coordinate, Double speed, Double trak, Integer altitude){
		this.icao = icao;
		this.operator = operator;
		this.species = species;
		this.posTime = posTime;
		this.coordinate = coordinate;
		this.speed = speed;
		this.trak = trak;
		this. altitude = altitude;
	}

	//TODO: Create relevant getter methods

	public String getIcao(){
		return this.icao;
	}

	public String getOperator() {
		return this.operator;
	}

	public Integer getSpecies() {
		return this.species;
	}

	public Date getPosTime() {
		return this.posTime;
	}

	public Coordinate getCoordinate() {
		return this.coordinate;
	}

	public Double getSpeed() {
		return this.speed;
	}

	public Double getTrak() {
		return this.trak;
	}

	public Integer getAltitude() {
		return this.altitude;
	}

	//TODO: Lab 4-6 return attribute names and values for table
	public static ArrayList<String> getAttributesNames()
	{
		ArrayList<String> al = new ArrayList<String>();
		al.clear();

		BasicAircraft ba = new BasicAircraft();

		Class c = ba.getClass();

		Field[] fields = c.getFields();

		for (Field f: fields) {
			String s = f.getName();
			al.add(s);
		}

		return al;
	}

	public static ArrayList<Object> getAttributesValues(BasicAircraft ac)
	{

		ArrayList<Object> al = new ArrayList<Object>();
		al.clear();

		al.add(ac.getIcao());
		al.add(ac.getOperator());
		al.add(ac.getSpecies());
		al.add(ac.getPosTime());
		al.add(ac.getCoordinate());
		al.add(ac.getSpeed());
		al.add(ac.getTrak());
		al.add(ac.getAltitude());


		return al;
	}

	//TODO: Overwrite toString() method to print fields
	public String toString(){
		return "Icao: " + this.icao + "Operator: " + this.operator + "Species: " + this.species + "posTime: " + this.posTime + "Coordinate: " + this.coordinate + "Speed: " + this.speed + "Trak: " + this.trak + "Altitude: " + this.altitude;
	}
}
