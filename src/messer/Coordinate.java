package messer;

public class Coordinate {
	private double latitude;
	private double longitude;
	
	//TODO: Constructor, Getter/Setter and toString()

	public Coordinate(double latitude, double longitude){
		this.latitude = latitude;
		this.longitude = longitude;
	}


	@Override
	public String toString() {
		return "Latitude" + this.latitude + "Longitude" + this.longitude;
	}
}